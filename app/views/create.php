<?php

    include_once 'partials/header.php';
    // var_dump($data['errors']);
?>

<form action="" method="" id="product_form">

    <!-- header -->
    <div class="d-flex bd-highlight my-3">

        <div class="p-2 bd-highlight">
            <h1 class="form-title">Product Add</h1>
        </div>
        <div class="ms-auto p-2 bd-highlight">
            <button id="save" type="submit" class="btn btn-outline-primary">Save</button>
            <a href="/" class="btn btn-outline-danger">Cancel</a>
        </div>

    </div>

    <hr>
    <div class="mt-5">


        <div class="form-row" style="color:#dc3545;" id="errorDiv"></div>


        <div class="form-group col-md-6 col-sm-12">
            <label for="">SKU</label>
            <br>
            <input type="text" name="sku" id="sku" value="<?php echo $_POST['sku']; ?>">
        </div>
        <br>

        <div class="form-group col-md-6 col-sm-12">
            <label for="">Name</label>
            <br>
            <input type="text" name="name" id="name" value="<?php echo $_POST['name']; ?>">
        </div>
        <br>

        <div class="form-group col-md-6 col-sm-12">
            <label for="">Price($)</label>
            <br>
            <input type="number" name="price" id="price" value="<?php echo $_POST['price']; ?>">
        </div>
        <br>

        <div class="form-group col-md-6 col-sm-12 d-flex">
            <label for="" class="me-5">Type Switcher</label>
            <br><br>
            <select name="type" id="productType" class="mb-5">
                <option value="DVD">DVD</option>
                <option value="Book">Book</option>
                <option value="Furniture">Furniture</option>
            </select>
        </div>


        <div id="attributes" class="form-group col-md-6 col-sm-12">
            <label for="">Size(MB)</label><br>
            <input type="number" name="size" id="size" value="<?php echo $_POST['size']; ?>">

            <br><br>
            <p>"product description"</p>
        </div>


    </div>

</form>
<?php



    include_once 'partials/footer.php';
?>