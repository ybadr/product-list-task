<?php

    session_start();

    $products = $data['products'];

    include_once 'partials/header.php';
?>


<form action="/delete" method="POST">


    <!-- header -->
    <div class="d-flex bd-highlight my-3">

        <div class="p-2 bd-highlight">
            <h1 class="form-title">Product List</h1>
        </div>
        <div class="ms-auto p-2 bd-highlight">
            <a class="btn btn-outline-primary" href="create">ADD</a>
            <button type="submit" class="float-end btn btn-outline-danger ms-3" id="delete-product-btn">MASS
                DELETE
            </button>
        </div>

    </div>

    <hr>



    <?php if(isset($_SESSION['success'])) {  echo "<p class='alert alert-success'>{$_SESSION['success']}</p>" ; } ?>

    


    <div class="row row-cols-lg-4 g-2 g-lg-3">

        <?php 
            if(!empty($products)) : foreach ($products as $product) :
        ?>


        <div class="col-lg-3 col-md-6 col-sm-12">
            <div class="box p-3 border bg-light">
                <input class="form-check-input delete-checkbox" type="checkbox" name="products[]"
                    value="<?php  echo $product['sku']; ?>" id="flexCheckDefault">
                <br>
                <div class="text-center">
                    <div><?php  echo $product['sku']; ?></div>
                    <div><?php  echo $product['name']; ?></div>

                    <div><?php  echo $product['price']; ?> $ </div>
                    <p><?php echo $product['attribute']; ?></p>

                </div>
            </div>
        </div>
        
        <?php 
            endforeach;
            else : 
                echo "<p class='text-center text-red'>No Products Found try to add one</p>";
            endif;
        ?>


    </div>
</form>

<?php 

    session_unset();
    include_once 'partials/footer.php';
?>