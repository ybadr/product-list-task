<?php

use app\core\Controller;
use app\models\Product;
use app\models\validator;

class home extends Controller {

    public function index($name = '') {

        $products = (new Product)->all();

        $this->view('index',[
            'products'=>$products
        ]);
    }

    public function create()
    {
        $this->view('create');
    }


    public function delete()
    {   
   
        if (!$_POST) {
            header("location:/");
        } else {
            $products = "'" . implode("','",$_POST['products'])  . "'";
            (new Product)->delete($products);

            header("location:/");
        }

    }

    public function store()
    {   

        //validation
        $errors = (new validator)->isEmpty($_POST);

        //check for empty fields
        if ($errors) {
            echo json_encode(['success'=>'0','msg'=>$errors]);
        } else {
            //check if sku exists
            $errors = (new Product)->skuExist($_POST['sku']);
            if ($errors) {
                echo json_encode(['success'=>'0','msg'=>$errors]);
            } else {
                //store the request
                (new Product)->store($_POST);
                $_SESSION['success'] = "New Product Added";
                echo json_encode(['success'=>'1']);
            }

        }
        
    }

}


