<?php




    namespace app\models;

    use app\models\types\typeInterface;

    class validator{


        public function isEmpty($data)
        {
            $errors=[];

            foreach($data as $key=>$val){
                if(empty($val)){
                    $errors[] = 'Product '.$key.' is required';        
                }
            }
        
            return $errors;
        }


        public function setAttribute(typeInterface $type, $data)
        {   
            return $type->setAttribute($data);
        }

    }
