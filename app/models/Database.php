<?php 

  namespace app\models;

  use PDOException;
  use PDO;


  

  class Database {

    private $conn;

    // DB Connect
    public function __construct()
    {
      $this->conn = null;

      try { 
        $this->conn = new PDO('mysql:host=localhost;dbname=product_list','admin','admin123');
        $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      } catch(PDOException $e) {
        echo 'Connection Error: ' . $e->getMessage();
      }
      
      // return $this->conn;
    }

    public function connect()
    {
      return $this->conn;
    }
    
  }

  // $db = new Database;
  // var_dump($db->connect());