<?php 

    namespace app\models;

    abstract class Model
    {   
        abstract public function all();
        abstract public function store($values);
        abstract public function delete($products);
        abstract public function skuExist($sku);

    }