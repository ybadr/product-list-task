<?php

    namespace app\models\types;

    interface typeInterface
    {
        public function setAttribute(array $data);
    }