<?php

    namespace app\models\types;

    class Furniture implements typeInterface
    {   
        public function setAttribute($data)
        {
            return "deminsions: {$data['height']}x{$data['width']}x{$data['length']} CM\n " ;
        }
    }