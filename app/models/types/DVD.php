<?php

    namespace app\models\types;

    class DVD implements typeInterface
    {   
        public function setAttribute($data)
        {
            return "Size: {$data['size']} MB\n";
        }
    }