<?php

namespace app\models;


class Product extends Model
{

        protected $pdo;

        public function __construct()
        {   
            $db = new Database;
            $this->pdo = $db->connect();
        }

        public function all()
        {   

            $stmt = $this->pdo->prepare('SELECT * FROM `products` ORDER BY created_at DESC');
            $stmt->execute();
            
            return $stmt->fetchAll();
        }

        public function store($data)
        {   
            $sql = "INSERT INTO `products` (`sku`, `name`, `price`, `type`, `attribute`) VALUES (:sku, :name, :price, :type, :attribute)";
            

            $stmt = $this->pdo->prepare($sql);

            $request = "app\\models\\types\\" . $data['type'];

            $attr = (new validator)->setAttribute(new $request, $data);

            $stmt->bindValue(':sku', $data['sku']);
            $stmt->bindValue(':name', $data['name']);
            $stmt->bindValue(':price', $data['price']);
            $stmt->bindValue(':type', $data['type']);
            $stmt->bindValue(':attribute', $attr);
            
            return $stmt->execute();


        }

        public function delete($products)
        {
            $sql = "DELETE FROM `products` WHERE sku IN ({$products})";
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute();    
        }

        public function skuExist($sku)
        {
            $errors = [];

            $stmt = $this->pdo->prepare("SELECT * FROM `products` WHERE `sku` = :sku");
            $stmt->bindValue(':sku',$sku);
            
            $stmt->execute();

            if ($stmt->rowCount() > 0) {
                $errors[] = 'sku is already exist';
            }

            return $errors;
        }

}
