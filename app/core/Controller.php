<?php

namespace app\core;


class Controller {

    protected function view($view, $data = []) {

        if (file_exists('../app/views/' . $view . '.php')) {

            require_once '../app/views/' . $view . '.php';
        }
    }

}
