$(document).ready(function() {
    $('#productType').change(function() {
        var id = $(this).val();
        switch (this.value) {
            case 'DVD':
                $("#attributes").html(`
                        <label for="">Size(MB)</label><br>
                        <input type="number" name="size" id="size">

                        <br><br>
                        <p>"product description"</p>
                    `);
                break;

            case 'Book':
                $("#attributes").html(`
                        <label for="">Weight(KG)</label><br>
                        <input type="number" id="weight" name="weight">

                        <br><br>
                        <p>"product description"</p>
                    `);
                break;

            case 'Furniture':
                $("#attributes").html(`
                        <label for="">Height(CM)</label><br>
                        <input type="number" id="height" name="height">
                        <br><br>
                        <label for="">Width(CM)</label><br>
                        <input type="number" id="width" name="width">
                        <br><br>

                        <label for="">Length(CM)</label><br>
                        <input type="number" id="length" name="length">
                        <br><br>

                        <p>"product description"</p>
                    `);
                break;
        }

    });

    $('#product_form').submit(function(e) {
        e.preventDefault();

        var content = '';
        $.ajax({
            type: "POST",
            url: "/store",
            data: $(this).serialize(),
            success: function(data) {
                let jsonData = JSON.parse(data);

                // console.log(jsonData.msg);

                if (jsonData.success == 0) {
                    (jsonData.msg).forEach(element => {
                        content += '<p>' + element + '</p>';
                    });
                    $('#errorDiv').html(content);
                }
                if (jsonData.success == 1) {
                    window.location.href = "/index.php";
                }
            }
        });
    })
});